SET
         I usuarios/DISTRI,JAVERIANA,EXITO,URBANIZACION/
         H horas/h1,h2,h3,h4,h5,h6,h7,h8,h9,h10,h11,h12/

TABLE
C(I,H) tabla de costos de variables de desconexi�n por hora

                    h1   h2   h3   h4   h5   h6   h7    h8   h9    h10  h11  h12
DISTRI             310  362  310  350  362  370   315  311   353   345  311  318
JAVERIANA          362  370  363  463  340  386   416  367   377   363  405  412
EXITO              562  440  460  435  470  454   450  464   411   453  465  415
URBANIZACION       235  263  296  310  235  297   237  282   211   320  236  250;



PARAMETER
F(I) COSTOS FIJOS
/DISTRI  200
JAVERIANA   250
EXITO    300
URBANIZACION 320/;

PARAMETER
D(H)                     DEMANDA DE DESCONEXION POR HORA EN KW
/h1        810
h2        1416
h3         958
h4         823
h5        1169
h6        1295
h7         835
h8        1625
h9        3206
h10       1029
h11       1076
h12       1836/;

PARAMETER
CAPMAX (I)                       CAPACIDAD MAXIMA DE DESCONEXI�N DE CADA USUARIO EN KW
/DISTRI  1100
JAVERIANA  1000
EXITO    1200
URBANIZACION 400/;

PARAMETER
CAPMIN(I)                        CAPACIDAD MINIMA DE DESCONEXION DE CADA USUARIO EN KW
/DISTRI  200
JAVERIANA  300
EXITO  300
URBANIZACION 50/;

PARAMETER
KMAX(I)                          NUMERO MAXIMO
/DISTRI  10
JAVERIANA    20
EXITO    12
URBANIZACION 8/;

VARIABLES
FO
COFI
P(I,H)                          POTENCIA QUE PUEDE DESCONECTAR EL USUARIO I EN LA HORA H
Z(I,H)                          SI SE DESCONECTA EL USUARIO I EN LA HORA H ENTONCES 1 SINO ENTONCES 0
V
POSITIVE VARIABLES P(I,H),V;
INTEGER VARIABLES Z(I,H);

EQUATIONS
OBJ
COSTEFIJO
*Restricciones
DEMANDA(H)                                              demanda de desconexi�n para cada hora
Veces                                                   n�mero global de participaciones del agregador
RESVecesdetodos                                         para el m�ximo global del agregador
Vecesuser (I)                                              maxima cantidad de veces por usuario
MAXdescon(I,H)                                         maxima cantidad DE POTENCIA que puede desconectarse en cada hora del dia
MAXdescon0(I,H)                                              cero si no es llamado el usuario i en determinada hora
MAXdescon1(I,H)                                           uno si es llamado el usuario i en determinada hora  ;
COSTEFIJO.. COFI =E= SUM((I,H), F(I)*Z(I,H));
OBJ.. FO =E= SUM((I,H), C(I,H)*P(I,H)) + COFI;
*An�lisis de las restricciones
DEMANDA(H).. SUM(I,P(I,H)) =G= D(H);
Veces..V=e=sum((H,I),Z(I,H));
RESVecesdetodos .. V =L= 40;
Vecesuser(I)..sum(H,Z(I,H))=L=KMAX(I);
MAXdescon0(I,H).. Z(I,H) =G= 0;
MAXdescon1(I,H).. Z(I,H) =L= 1;
MAXdescon(I,H).. P(I,H) =L= (CAPMAX(I))*Z(I,H);

MODEL LOCALIZ /ALL/;
SOLVE LOCALIZ USING MIP MINIZING FO;
      DISPLAY
 DEMANDA.L,P.L,Z.L,V.L,Vecesuser.L,FO.L;
